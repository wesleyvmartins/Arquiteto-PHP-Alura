<?php

namespace Alura\DesignPattern\Log;

class StdountLogManager extends LogManager
{
    public function criarLogWritter(): LogWritter
    {
        return new StdoutLogWritter();
    }
}