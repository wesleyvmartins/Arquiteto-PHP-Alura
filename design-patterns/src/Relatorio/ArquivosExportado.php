<?php

namespace Alura\DesignPattern\Relatorio;

interface ArquivosExportado
{
    public function salvar(ConteudoExportado $conteudoExportado): string;
}